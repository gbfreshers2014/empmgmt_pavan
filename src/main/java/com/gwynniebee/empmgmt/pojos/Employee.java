/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.empmgmt.pojos;

/**
 * @author pavan
 */
public class Employee {

    private int empId;
    private String emailId;
    private String fName;
    private int phone;
    private int status;

    /**
     * @return employee Id
     */
    public int getEmpId() {
        return empId;
    }

    /**
     * @param empId sets Employee Id
     */
    public void setEmpId(int empId) {
        this.empId = empId;
    }

    /**
     * @return email Id of Employee
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * @param emailId sets employee email Id
     */
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    /**
     * @return Employee Name
     */
    public String getfName() {
        return fName;
    }

    /**
     * @param fName sets Name
     */
    public void setfName(String fName) {
        this.fName = fName;
    }

    /**
     * @return phone number of employee
     */
    public int getPhone() {
        return phone;
    }

    /**
     * @param phone sets phone number
     */
    public void setPhone(int phone) {
        this.phone = phone;
    }

    /**
     * @return status of the employee
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status sets status of employee
     */
    public void setStatus(int status) {
        this.status = status;
    }

}
