/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.empmgmt.em;

import org.skife.jdbi.v2.DBI;

/**
 * @author pavan
 */
public abstract class BaseEntityManager {

    /**
     * Default constructor.
     */
    protected BaseEntityManager() {
    }

    private static DBI dbi;

    /**
     * @param dbi the dbi to set
     */
    public static void setDbi(DBI dbi) {
        BaseEntityManager.dbi = dbi;
    }

    /**
     * @return the dbi
     */
    public static DBI getDbi() {
        return dbi;
    }
}
