/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.empmgmt.em;

import java.util.List;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.empmgmt.dao.EmployeeDAO;
import com.gwynniebee.empmgmt.pojos.Employee;
import com.gwynniebee.empmgmt.restlet.EmpMgmtApplication;

/**
 * @author pavan
 */
public class EmployeeeEntityManager extends BaseEntityManager {
    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(EmployeeeEntityManager.class);
    private static final EmployeeeEntityManager MANAGER_INSTANCE = new EmployeeeEntityManager();

    /**
     * @return employee entity instance
     */
    public static EmployeeeEntityManager getInstance() {
        return MANAGER_INSTANCE;
    }

    /**
     * @return list of employees
     */
    public List<Employee> getEmployeesDetail() {
        EmployeeDAO dao = null;
        Handle h = null;
        LOG.debug("before getdbi");
        DBI dbi = EmployeeeEntityManager.getDbi();
        LOG.debug("after getdbi");
        h = dbi.open();
        LOG.debug("after open");
        h.begin();
        LOG.debug("trying to attach ItemDAO");

        dao = h.attach(EmployeeDAO.class);
        LOG.debug("done attaching ItemDAO");
        try {
            LOG.debug("before DAO");
            dao = dbi.open(EmployeeDAO.class);

            List<Employee> employees = dao.getEmployeesDetails();
            LOG.debug(" In DAO before return items for" + employees.toString());
            return employees;

        } finally {
            if (dao != null) {
                // h.close();
                dao.close();
            }
        }
    }

    /**
     * @param ee employee object
     * @return record id
     */
    public int addEmployee(Employee ee) {

        EmployeeDAO dao = null;
        Handle h = null;
        DBI dbi = EmpMgmtApplication.getDbi();
        h = dbi.open();
        h.begin();
        dao = h.attach(EmployeeDAO.class);
        dao = dbi.open(EmployeeDAO.class);
        LOG.debug(" In Employee DAO before return : ");
        int ret = 0;
        try {

            ret = dao.addEmployee(ee.getEmpId(), ee.getEmailId(), ee.getfName(), ee.getPhone(), ee.getStatus());
            LOG.debug("employeeId : " + ret);
           // dao.commit();
            // h.commit();
            h.close();

        } finally {
            if (dao != null) {
                h.close();
            }

        }
        return ret;

    }

    /**
     * @param eId of employee
     * @return number of records deleted
     */
    public int deleteEmployee(int eId) {
        // int eId=Integer.parseInt(empId);
        EmployeeDAO dao = null;
        Handle h = null;
        DBI dbi = EmpMgmtApplication.getDbi();
        h = dbi.open();
        h.begin();
        dao = h.attach(EmployeeDAO.class);
        dao = dbi.open(EmployeeDAO.class);
        LOG.debug(" In Employee DAO before return : ");
        int ret = 0;
        try {

            ret = dao.deleteEmployee(eId);
            LOG.debug("employeeId : " + ret);
            dao.commit();
            h.commit();
            h.close();

        } finally {
            if (dao != null) {
                h.close();
            }

        }
        return ret;
    }

}
