/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.empmgmt.j2ee;

import com.gwynniebee.empmgmt.restlet.EmpMgmtApplication;
import com.gwynniebee.rest.service.j2ee.GBRestletServlet;

/**
 * @author pavan
 */
@SuppressWarnings("serial")
public class RestfulServlet extends GBRestletServlet<EmpMgmtApplication> {
    /**
     * This is called by tomcat's start of servlet.
     */
    public RestfulServlet() {
        this(new EmpMgmtApplication());
    }

    /**
     * This is called by unit test which create a subclass of this class with
     * subclass of application.
     * @param app application
     */
    public RestfulServlet(EmpMgmtApplication app) {
        super(app);
    }
}
